pipeline {
    agent any

    environment {
         DOCKER_BIN = '/usr/local/bin/docker'
        DOCKER_IMAGE = 'oma09/website_karma'
        IMAGE_TAG = 'latest'
        REGISTRY = 'https://index.docker.io/v1/'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login' // Use the credentials ID directly
        HEROKU_APP_NAME = 'websitekarma3'
    }

    stages {
        stage('Build image') {
            steps {
                script {
                    // Building the Docker image with the AMD64 platform support
                    sh "${DOCKER_BIN} build --platform linux/amd64 -t ${DOCKER_IMAGE}:${IMAGE_TAG} ."
                }
            }
        }

        stage('Test image') {
            steps {
                script {
                    // Test the Docker image by running a container and checking for expected content
                    sh "${DOCKER_BIN} rm -f web || true"
                    sh "${DOCKER_BIN} run --name web -d -p 5002:80 ${DOCKER_IMAGE}:${IMAGE_TAG}"
                    sh "sleep 10"
                    sh "curl -s http://localhost:5002 | grep -q '<h1>Nike New <br>Collection!</h1>'"
                }
            }
        }

        stage('Push image to Docker Hub') {
            steps {
                script {
                    // Pushing the Docker image to Docker Hub
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'DOCKERHUB_USER', passwordVariable: 'DOCKERHUB_PASS')]) {
                        sh "echo $DOCKERHUB_PASS | ${DOCKER_BIN} login --username $DOCKERHUB_USER --password-stdin ${REGISTRY}"
                        sh "${DOCKER_BIN} push ${DOCKER_IMAGE}:${IMAGE_TAG}"
                    }
                }
            }
        }

        stage('Deploy to Heroku') {
            steps {
                script {
                    // Deploying the Docker image to Heroku
                    withCredentials([string(credentialsId: 'heroku_api_key', variable: 'HEROKU_API_KEY')]) {
                        sh """
                        ${DOCKER_BIN} tag ${DOCKER_IMAGE}:${IMAGE_TAG} registry.heroku.com/${HEROKU_APP_NAME}/web
                        echo $HEROKU_API_KEY | ${DOCKER_BIN} login --username=_ --password-stdin registry.heroku.com
                        ${DOCKER_BIN} push registry.heroku.com/${HEROKU_APP_NAME}/web
                        heroku container:release web --app ${HEROKU_APP_NAME}
                        """
                    }
                }
            }
        }

        stage('Check Deployment') {
            steps {
                script {
                    // Checking if the deployed application is live and accessible
                    sh "curl -s https://${HEROKU_APP_NAME}.herokuapp.com"
                }
            }
        }
    }

    post {
        always {
            // Cleaning up Docker containers post-build
            sh "${DOCKER_BIN} stop web || true"
            sh "${DOCKER_BIN} rm web || true"
            
            // Sending build completion email
            emailext(
                to: 'carnelilordwiz@gmail.com',
                subject: "Build ${currentBuild.fullDisplayName} completed",
                body: """<html><body>
                         <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> was completed.</p>
                         <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
        success {
            echo 'Build and deployment were successful!'
        }
        failure {
            echo 'Build or deployment failed.'
        }
    }
}