# Use a PHP and Apache base image
FROM php:7.4-apache

# Install necessary packages and perform clean-up in one layer to reduce image size
# Ensure curl is installed for health checks
RUN apt-get update && apt-get install -y \
    bash \
    libpng-dev \
    curl \
    && docker-php-ext-install gd pdo_mysql \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Copy your project files into the Apache document root
COPY . /var/www/html/

# Change ownership and permissions for security and operational efficiency
RUN chown -R www-data:www-data /var/www/html \
    && find /var/www/html -type d -exec chmod 755 {} \; \
    && find /var/www/html -type f -exec chmod 644 {} \;

# Create a non-root user 'jostino' for additional security
RUN adduser --disabled-password --gecos "" jostino \
    && chown -R jostino:jostino /var/www/html

# Create a script to modify Apache configuration based on $PORT
RUN echo '#!/bin/bash\n\
sed -i "s/Listen 80/Listen ${PORT}/" /etc/apache2/ports.conf\n\
sed -i "s/<VirtualHost \*:80>/<VirtualHost \*:${PORT}>/" /etc/apache2/sites-available/000-default.conf\n\
exec apache2-foreground' > /usr/local/bin/start-apache

# Make the script executable
RUN chmod +x /usr/local/bin/start-apache

# Configure to run as the www-data user
USER www-data

# Although EXPOSE is not used by Heroku, it's good practice to include it for documentation purposes
EXPOSE 80

# Add a health check for container health monitoring
HEALTHCHECK --interval=30s --timeout=3s --retries=3 CMD curl --fail http://localhost:$PORT/ || exit 1

# Use the custom script to start Apache
CMD ["/usr/local/bin/start-apache"]